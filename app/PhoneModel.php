<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneModel extends Model
{
    protected $fillable = [
        'model_name', 'mrp', 'status', 'added_by',
    ];
}
